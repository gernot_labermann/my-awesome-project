#!/bin/bash

set -euxo pipefail

cd "$(dirname "$0")"

# see https://carvel.dev/kapp-controller/docs/v0.38.0/walkthrough/

kapp deploy -a kc -f https://github.com/vmware-tanzu/carvel-kapp-controller/releases/latest/download/release.yml

kapp deploy -a default-ns-rbac -f https://raw.githubusercontent.com/vmware-tanzu/carvel-kapp-controller/develop/examples/rbac/default-ns.yml
kapp deploy -a my-app -f ./app.yml
